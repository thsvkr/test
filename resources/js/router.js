import Vue from 'vue'
import Router from 'vue-router'
import Order from "./components/views/Order";

Vue.use(Router);

let router = new Router({
  routes: [
    {
      path: '*',
      component: Order
    }
  ]
});

export default router
