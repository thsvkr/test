import "babel-polyfill";
import 'array-flat-polyfill';
import Vue from 'vue'
import Vuetify from 'vuetify'
import router from './router'
import App from "./App";
import Order from "./components/views/Order";

Vue.use(Vuetify);

new Vue({
  el: '#app',
  router,
  components: {
      App,
    Order
  },
  render: h => h(App)
}).$mount('#app');
