<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('plans', [
    'as' => 'plans.index',
    'uses' => 'Api\PlansController@index',
]);

Route::post('orders', [
    'as' => 'orders.store',
    'uses' => 'Api\OrdersController@store',
]);
