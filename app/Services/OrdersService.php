<?php

namespace App\Services;

use App\Models\Order;
use App\Repositories\ClientsRepository;
use App\Repositories\OrdersRepository;

class OrdersService
{
    private OrdersRepository $ordersRepository;
    private ClientsRepository $clientsRepository;

    public function __construct(
        OrdersRepository $ordersRepository,
        ClientsRepository $clientsRepository
    ) {
        $this->ordersRepository = $ordersRepository;
        $this->clientsRepository = $clientsRepository;
    }

    /**
     * @param array $attributes
     * @return Order
     */
    public function store(array $attributes): Order
    {
        $attributes['client_id'] = $this->clientsRepository->store($attributes)->getKey();

        $order = $this->ordersRepository->store($attributes);
        $order->load('plan', 'firstDay');

        return $order;
    }
}