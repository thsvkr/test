<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;

class CheckAdmin
{
    /**
     * Айди админов
     */
    const ADMIN_IDS = [
        50,
        54,
        64,
        68,
        2435,
        92031,
        515386,
        548566,
        87380,
        527807,
        69
    ];

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws AuthenticationException
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if (!$user || (env('APP_ENV') !== 'develop' && !\in_array($user->getKey(), self::ADMIN_IDS, true))) {
            Auth::logout();
            throw new AuthenticationException('User is not admin');

        }
        return $next($request);
    }
}
