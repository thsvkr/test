<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout', 'login']]);
    }

    public function logout()
    {
        //В случае выхода редиректим на logout маршрут auth сервиса
        $redirectUrl = config('microservices.auth.url') . '/logout';

        return redirect($redirectUrl);
    }

    public function login(Request $request)
    {
        $redirectUrl = config('microservices.auth.url');

        return redirect($redirectUrl);
    }
}
