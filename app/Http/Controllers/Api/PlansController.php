<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\PlanResource;
use App\Models\Plan;

class PlansController
{
    public function index()
    {
        return PlanResource::collection(Plan::with('days')->get());
    }
}