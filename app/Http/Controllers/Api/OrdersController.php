<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Orders\StoreRequest;
use App\Http\Resources\OrderResource;
use App\Services\OrdersService;

class OrdersController extends Controller
{
    private OrdersService $ordersService;

    public function __construct(OrdersService $ordersService)
    {
        $this->ordersService = $ordersService;
    }

    public function store(StoreRequest $request)
    {
//        sleep(7);
        $attributes = $request->validated();

        return OrderResource::make($this->ordersService->store($attributes));
    }
}