<?php

namespace App\Http\Resources;

use App\Models\Plan;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class PlanResource
 * @package App\Http\Resources
 *
 * @mixin Plan
 */
class PlanResource extends Resource
{
    public function toArray($request)
    {
        return [
            'plan_id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'days' => DayResource::collection($this->whenLoaded('days')),
        ];
    }
}