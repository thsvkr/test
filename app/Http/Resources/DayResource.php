<?php

namespace App\Http\Resources;

use App\Models\Day;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class DayResource
 * @package App\Http\Resources
 *
 * @mixin Day
 */
class DayResource extends Resource
{
    public function toArray($request)
    {
        return [
            'day_id' => $this->id,
            'name' => $this->name,
        ];
    }
}