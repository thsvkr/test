<?php

namespace App\Http\Resources;

use App\Models\Order;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class OrderResource
 * @package App\Http\Resources
 *
 * @mixin Order
 */
class OrderResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_day' => DayResource::make($this->whenLoaded('firstDay')),
            'plan' => PlanResource::make($this->whenLoaded('plan')),
        ];
    }
}