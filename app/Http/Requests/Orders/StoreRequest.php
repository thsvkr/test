<?php

namespace App\Http\Requests\Orders;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'phone_number' => 'required|string|regex:/^7\d{10}$/',
            'name' => 'required|string',
            'plan_id' => 'required|integer|exists:plans,id',
            'first_day_id' => 'required|integer|exists:days,id',
        ];
    }

    public function messages()
    {
        return [];
    }
}