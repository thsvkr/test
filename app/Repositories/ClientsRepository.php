<?php

namespace App\Repositories;

use App\Models\Client;

class ClientsRepository
{
    /**
     * @param array $attributes
     * @return Client
     */
    public function store(array $attributes): Client
    {
        return Client::firstOrCreate(
            ['phone_number' => $attributes['phone_number']], ['name' => $attributes['name']]
        );
    }
}