<?php

namespace App\Repositories;

use App\Models\Order;

class OrdersRepository
{
    /**
     * @param array $attributes
     * @return Order
     */
    public function store(array $attributes): Order
    {
        $order = new Order();

        $order->fill($attributes);
        $order->save();

        return $order;
    }
}