<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Plan
 * @package App\Models
 *
 * @property-read int $id
 * @property string $name
 * @property int $price
 * @property Collection $days
 */
class Plan extends Model
{
    public function days()
    {
        return $this->belongsToMany(Day::class, 'plan_days', 'plan_id', 'day_id');
    }
}