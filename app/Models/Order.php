<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Order
 * @package App\Models
 *
 * @property-read int $id
 * @property Client $client
 * @property Plan $plan
 * @property Day $firstDay
 */
class Order extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'plan_id',
        'first_day_id',
        'client_id',
    ];

    /**
     * @return HasOne
     */
    public function client(): HasOne
    {
        return $this->hasOne(Client::class, 'id', 'client_id');
    }

    /**
     * @return HasOne
     */
    public function plan(): HasOne
    {
        return $this->hasOne(Plan::class, 'id', 'plan_id');
    }

    /**
     * @return HasOne
     */
    public function firstDay(): HasOne
    {
        return $this->hasOne(Day::class, 'id', 'first_day_id');
    }
}