<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Client
 * @package App\Models
 *
 * @property-read int $id
 * @property string $phoneNumber
 * @property string $name
 *
 * @mixin Builder
 */
class Client extends Model
{
    protected $fillable = [
        'phone_number',
        'name'
    ];
}