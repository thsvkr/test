<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Day
 * @package App\Models
 *
 * @property-read integer $id
 * @property string $name
 */
class Day extends Model
{
    protected $table = 'days';
}