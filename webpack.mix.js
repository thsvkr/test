let mix = require('laravel-mix');
let aliases = require('./alias.webpack');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.webpackConfig({
//     module: {
//         rules: [
//             {
//                 test: /\.jsx?$/,
//                 use: [
//                     {
//                         loader: 'babel-loader',
//                         options: {
//                             presets: ['es2015', 'latest', 'stage-2'] // default = env
//                         }
//                     }
//                 ]
//             }
//         ]
//     }
// });

//mix.js('resources/assets/js/app.js', 'public/js')
//   .sass('resources/assets/sass/app.scss', 'public/css');

mix.js('resources/js/app.js', 'public/js')
  .sass('resources/sass/app.scss', 'public/css')
  .stylus('node_modules/vuetify/src/stylus/main.styl', 'public/css')
  .webpackConfig(aliases);
