<?php

return [
    'auth' => [
        'url' => rtrim(env('SBS_AUTH_URL', 'https://auth.sberbank-school.ru'), '/')
    ]
];
