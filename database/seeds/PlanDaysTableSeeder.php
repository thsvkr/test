<?php


class PlanDaysTableSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('plan_days')->insert(
            [
                [
                    'plan_id' => 1,
                    'day_id' => 1,
                ],
                [
                    'plan_id' => 1,
                    'day_id' => 3,
                ],
                [
                    'plan_id' => 1,
                    'day_id' => 5,
                ],
                [
                    'plan_id' => 2,
                    'day_id' => 1,
                ],
                [
                    'plan_id' => 2,
                    'day_id' => 2,
                ],
                [
                    'plan_id' => 2,
                    'day_id' => 3,
                ],
                [
                    'plan_id' => 3,
                    'day_id' => 6,
                ],
                [
                    'plan_id' => 3,
                    'day_id' => 7,
                ],
            ]
        );
    }
}