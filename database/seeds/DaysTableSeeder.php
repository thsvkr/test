<?php

class DaysTableSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $names = [
            'Понедельник',
            'Вторник',
            'Среда',
            'Четверг',
            'Пятница',
            'Суббота',
            'Воскресенье',
        ];

        foreach ($names as $name) {
            $day = new \App\Models\Day(['name' => $name]);
            $day->save();
        }
    }
}