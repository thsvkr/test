<?php

class PlansTableSeeder extends \Illuminate\Database\Seeder
{
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 3; $i++) {
            $plan = new \App\Models\Plan();
            $plan->name = $faker->name;
            $plan->price = 100 * $i;
            $plan->save();
        }
    }
}